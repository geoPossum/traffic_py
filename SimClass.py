# file is imported, do not use as is
import random
import logging
import os,sys

# globals used
start_time_hours_offset = 11
time = 0
end_time=1

logger = logging.getLogger('SimClass')
logger.setLevel(logging.DEBUG)
file_handler = logging.FileHandler('log.txt', mode='w')
file_handler.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(name)s - %(levelname)s - %(message)s")
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

# a dictionary of a route and the stops along it,
#sorted by id of the route
global_routes = {}
bus_stops = []
psv_s = []
# major sinks are along the main road
major_sinks = []
sinks = []

sink_weight_multiplier = 0
stop_weight_multiplier = 0

#define bus capacity
MATATU_CAPACITY = 14
SMALL_BUS_CAPACITY = 25
BIG_BUS_CAPACITY = 51
hours_to_run = 3

#time matatu stays at a Bus Stop in an estate in minutes
estate_time_config = {
    'max_psvs_at_station'             :  1,  # maximum number of psvs allowed to park at a station
    'MATATU_WAIT_TIME'               :  5*60,
    'SMALL_BUS_WAIT_TIME'            :  8*60,
    'BIG_BUS_WAIT_TIME'              :  10*60,
    'MATATU_TIME_BETWEEN_STATIONS'   :  6*60,
    'SMALL_BUS_BETWEEN_STATIONS'     :  12*60,
    'BIG_BUS_BETWEEN_STATIONS'       :  15*60
}

highway_time_config = {
    'max_psvs_at_station'             :  5,  # maximum number of psvs allowed to park at a station
    'MATATU_WAIT_TIME'               :  2*60,
    'SMALL_BUS_WAIT_TIME'            :  3*60,
    'BIG_BUS_WAIT_TIME'              :  5*60,
    'MATATU_TIME_BETWEEN_STATIONS'   :  30*60,
    'SMALL_BUS_BETWEEN_STATIONS'     :  33*60,
    'BIG_BUS_BETWEEN_STATIONS'       :  35*60
}


def convert_time(time):
    hours = time/3600
    minutes = (time%3600)/60
    string = "AM"
    if hours/12:
        hours %= 12
        if hours == 0:
            hours = 12
        string= "PM"

    return "{0}h:{1}m:{2}s {3}".format(hours, minutes, time%60, string)


def run_sim():
    global time, end_time

    time = 0 + start_time_hours_offset*3600

    end_time = hours_to_run*3600 + start_time_hours_offset*3600
    while time<end_time:
        for stop in bus_stops:
            stop.run()
        for psv in psv_s:
            psv.run()

        # print("Time is now: %s" % time)
        logger.info('Global Time: %s : %s', time, convert_time(time) )
        time+=1