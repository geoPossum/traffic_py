# file is imported, do not use as is
import random
import SimClass as SCL
import SimStats
import logging
import os, sys

global_station_statistics = SimStats.global_station_statistics

logger = logging.getLogger('simDef')
logger.setLevel(logging.DEBUG)
logger.addHandler(SCL.file_handler)

def convert_time(time):
	hours = time/3600
	minutes = (time%3600)/60
	string = "AM"
	if hours/12:
		hours %= 12
		if hours == 0:
			hours = 12
		string= "PM"

	return "{0}h:{1}m:{2}s {3}".format(hours, minutes, time%60, string)

def set_sink_multiplier():
	"""
	Makes all weights in SCL.major_sinks whole numbers and returns multiplier

	Called as last thing, once set, you cannot add any more sinks since it tunrns SCL.major_sinks into a tuple."""
	# get the longest decimal place for all weights
	longest_decimals = 0

	for sink in SCL.major_sinks:
		weight = sink.weight
		split =  str(weight).split(".")
		if len(split)>1:
			# contains decimal places
			length = len(split[-1])
			if  length>longest_decimals:
				longest_decimals = length

	# set the multipler
	multipler = pow(10, longest_decimals)
	# convert all the weights to make them whole numbers
	temp = []
	for sink in SCL.major_sinks:
		sink.weight= int(sink.weight*multipler)

	# convert major_sinks to tuple to prevent additions
	temp = SCL.major_sinks
	SCL.major_sinks = tuple(temp)
	return multipler

def assign_psvs():
	# routes are keys and the next station to assign a psv is the value
	temp_routes= {}

	for route in SCL.global_routes:
		if route not in temp_routes.keys():
			temp_routes[route] = 0
		for psv in SCL.psv_s:
			if psv.route == route:
				next_empty = temp_routes[route]
				psv.station_index = next_empty
				# tell psv it is at a station
				psv.at_station = True
				logger.info("{0} starting at {1}".format(psv, SCL.global_routes[route][next_empty] ))
				# tell station there is a psv there
				SCL.global_routes[route][next_empty].psvs_at_station.append(psv) 
				temp_routes[route] = (next_empty+1)%len(SCL.global_routes[route])

class Passenger(object):
	id_ = 0

	def __init__(self, source, destination, spawn_time):
		self.source = source
		self.id = Passenger.id_
		Passenger.id_ += 1 
		self.destination = destination
		self.passed_through = []
		self.spawn_time = spawn_time

	def __str__(self):
		return "Passenger#{0} src:{1} dest:{2} spawnTime:{3}".format(self.id, self.source, self.destination, SCL.convert_time(self.spawn_time) )

class PSV(object):
	class Full(Exception):
		def __init__(self, message="Matatu is full"):
			self.message = message

		def __str__(self):
			return self.message

	def __init__(self, capacity, route, ):
		self.seating_area = []
		self.capacity = capacity
		self._iter_index = None
		id_ = len(SCL.psv_s)
		# a unique integer to identify each psv in the whole system
		self.id_ = id_
		SCL.psv_s.append(self)
		self.route = route
		self.at_station = False
		self.station_index = None
		# the below are actually replaced by deriving classes
		self.time_at_station = self.max_waiting_time = 0
		self.max_time_between_stations = self.time_between_stations = 0

		# used to create extra wait for matatus on main route
		self.wait_register = 0 

	def run(self):
		"""This method is only for managing a psv getting to a station, adding itself to the list of psvs
		waiting at that particular station, remove the psv from the station's list of psvs and keeping track
		of how much time is left to for the matatu to arrive at the next station.

		The station itself is used for the loading and unloading."""
		exit_station = False
		waiting_to_enter_station = True
		# if we arrived at a station
		if self.time_between_stations < 1:
			self.at_station = True
			#set psv station attribute to the index of the new one we have arrived at
			self.station_index = (self.station_index+1)%len(SCL.global_routes[self.route])
			#add this psv to the station's waiting area attribute ta=hat is a list
			station =SCL.global_routes[self.route][self.station_index]
			logger.info('{0} arrived at {1} capacity {3} time:{2}'.format(self, station, SCL.convert_time(SCL.time), len(self)))
			# print len(station.psvs_at_station)
			# station.psvs_at_station.append(self)	
			station.psvs_at_station.append(self)

			# reset the amount of time we have spent at the station and also travel time
			self.time_at_station = self.max_waiting_time
			self.time_between_stations = self.max_time_between_stations
			
		if self.at_station:
			# #waiting time expired therefore leave station
			# if self.time_at_station < 1:
			# 	exit_station = True
			# 	station = SCL.global_routes[self.route][self.station_index]

			# 	logger.info('{0} time expired at {1} time:{2}'.format(self, station, SCL.convert_time(SCL.time)))

			# #we are not full -> reduce waiting time
			# elif len(self.seating_area) < self.capacity:
			# 	self.time_at_station -= 1

			# if we are at a main_station, we leave
			station = SCL.global_routes[self.route][self.station_index]
			if isinstance(station, EstateMainStop):
				if not self.wait_register:
					# Ensure we leave at the next 2 rounds of time / seconds
					self.wait_register = 2
				else:
					if self.wait_register == 1:
						exit_station = True
						logger.info('{0} Leaving a main station: {1}'.format(self, station))

					self.wait_register-=1


			#for a non main station, we are full-> leave and reset time at station and the next station
			if len(self.seating_area) == self.capacity:
				station = SCL.global_routes[self.route][self.station_index]
				if not self.wait_register:

					# Ensure we leave at the next 2 rounds of time / seconds
					self.wait_register = 2
				else:
					if self.wait_register == 1:
						exit_station = True
						logger.info('{0} Full at {1}'.format(self, station))

					self.wait_register-=1

		#if we are not at a station:
		else:
			#reduce time to next station 
			self.time_between_stations -=1

		if exit_station:
			self.at_station = False
			station = SCL.global_routes[self.route][self.station_index]
			#remove self (psv) from station
			if self in station.psvs_at_station:
				index = station.psvs_at_station.index(self)
				del station.psvs_at_station[index]

	def load(self,passenger):
		if len(self.seating_area)<self.capacity:
			self.seating_area.append(passenger)
		else:
			raise PSV.Full

	def unload(self,index):
		return self.seating_area.pop(index)

	def next(self):
		# returns tuple of index of where passenger is seated or 
		if self._iter_index:
			self._iter_index-=1
			return self._iter_index, self.seating_area[self._iter_index]
		else:
			self._iter_index = None
			raise StopIteration

	def __iter__(self):
		self._iter_index = len(self.seating_area)
		return self

	def __str__(self):
		return "PSV:{0}".format(self.id_) # return "PSV:{0}:{1}".format(self.id_, self.route)

	def __len__(self):
		return len(self.seating_area)

class Matatu(PSV):
	def __init__(self, route, time_between_stations):
		super(Matatu, self).__init__( SCL.MATATU_CAPACITY, route)
		if self.route == "MAIN":
			self.time_at_station = self.max_waiting_time = SCL.highway_time_config['MATATU_WAIT_TIME']
			self.max_time_between_stations = self.time_between_stations = SCL.highway_time_config['MATATU_TIME_BETWEEN_STATIONS']
		else:
			self.time_at_station = self.max_waiting_time = SCL.estate_time_config['MATATU_WAIT_TIME']
			self.max_time_between_stations = self.time_between_stations = time_between_stations

class SmallBus(PSV):
	def __init__(self, route):
		super(SmallBus, self).__init__( SCL.SMALL_BUS_CAPACITY, route)
		if self.route == "MAIN":
			self.time_at_station = self.max_waiting_time = SCL.highway_time_config['SMALL_BUS_WAIT_TIME']
			self.max_time_between_stations = self.time_between_stations = SCL.highway_time_config['SMALL_BUS_BETWEEN_STATIONS']
		else:
			self.time_at_station = self.max_waiting_time = SCL.estate_time_config['SMALL_BUS_WAIT_TIME']
			self.max_time_between_stations = self.time_between_stations = SCL.estate_time_config['SMALL_BUS_BETWEEN_STATIONS']

class BigBus(PSV):
	def __init__(self, route):
		super(BigBus, self).__init__( SCL.BIG_BUS_CAPACITY, route)
		if self.route == "MAIN":
			self.time_at_station = self.max_waiting_time = SCL.highway_time_config['BIG_BUS_WAIT_TIME']
			self.max_time_between_stations = self.time_between_stations = SCL.highway_time_config['SMALL_BUS_BETWEEN_STATIONS']
		else:
			self.time_at_station = self.max_waiting_time = SCL.estate_time_config['SMALL_BUS_WAIT_TIME']
			self.max_time_between_stations = self.time_between_stations = SCL.estate_time_config['BIG_BUS_BETWEEN_STATIONS']

# todo: Model passenger and have them occupy traffic

class TimeGraph:
	# for a source of weight one, these are the values of generates
	# weight 1 would be an average stage in an estate of about 2000 workers
	adjusted_weight = 1
	graph = {
		'0': 0, 
		'1': 0, 
		'2': 0, 
		'3': 0, 
		'4': 0, 
		'5': 60, 
		'6': 300, 
		'7': 600, 
		'8': 400, 
		'9': 150, 
		'10': 100, 
		'11': 50, 
		'12': 50, 
		'13': 20, 
		'14': 10, 
		'15': 10, 
		'16': 30, 
		'17': 50, 
		'18': 50, 
		'19': 20, 
		'20': 10, 
		'21': 2, 
		'22': 2, 
		'23': 0
		}
	@classmethod
	def get_wait_time(cls):
		'''returns the number of seconds to wait based on time of day'''
		#convert time to hours
		hours = int(SCL.time/3600) % 24
		# linearly interpolate between the 10 minutes and return the number of people between this hour and the next
		left_index =  TimeGraph.graph[str(hours)] * TimeGraph.adjusted_weight
		right_index = TimeGraph.graph[str( (hours+1)%24 )] * TimeGraph.adjusted_weight
		# y= mx + c where c = 0
		m = (right_index-left_index)/3600.0
		x = SCL.time%3600
		
		no_of_passengers = random.triangular( left_index, left_index + m*(x+5*60) )
		# sys.exit(1)

		if no_of_passengers:
		# return the wait tim
			wait_time = int(3600/no_of_passengers)
			return wait_time
		else:
			# return one hour
			return 3600

	@classmethod
	def get_passenger(cls):
		args=TimeGraph.get_wait_triangular_args()

class EstateBusStop(object):
	def __init__(self, name, route,weight=0.5, index=None):
		"""Name shall be used in logging and printing output. 
		Route is a string literal that will be used to uniquely identify routes. No 2 routes can have the same route name.
		Index is a number unique within a route that is used to sort how routes follow each other."""
		self.weight = weight
		self.name = name
		self.waiting_area = []
		self._id = len(SCL.bus_stops)
		
		self.psvs_at_station = []
		self.time_to_next_spawn = 0

		self.spawns = 0
		self.route = route

		# statistic tracking object
		self.station_statistic_tracker= SimStats.StationStat(str(self))

		SCL.bus_stops.append(self)

		self.station_statistic = SimStats.StationStat()

		# add self to global routes
		if route not in SCL.global_routes:
			SCL.global_routes[route] = []

		# index is used to sort stops according to how one would like the matatus to go round in the estate
		if index:
			self.index= index
		else:
			self.index = len(SCL.global_routes[route])
		SCL.global_routes[route].append(self)

	def run(self):
		"""This method is responsible for spawning passengers randomly and placing passengers on the psvs that arrive at the station."""
		# SCL.major_sinks is a list of sinks with each having attribute weight
		# that is a relative factor of how many people that sink takes
		sum_of_major_weights = sum([ (sink.weight) for sink in SCL.major_sinks]) 
		sum_of_stop_weights = sum([ (stop.weight) for stop in SCL.bus_stops])

		if self.time_to_next_spawn < 1:
			# it is time to spawn a passengers
			# todo: Make choices based on weights
			destination = random.choice(SCL.major_sinks)
			# we register attribute SCL.time so that we can later record time of departure
			passenger = Passenger( self, destination, SCL.time  )
			self.waiting_area.append( passenger )
			logger.info("Spawned {1}".format(self, str(passenger), self.weight), )
			# todo: Change this to generate based on real time of day
			self.time_to_next_spawn = int(TimeGraph.get_wait_time()* (1/self.weight))

			logger.info("{0},next at {1}, total:{2}".format(self, SCL.convert_time(SCL.time+self.time_to_next_spawn), len(self.waiting_area) ) )

		# ensure that there are still matatus at station
		if len(self.psvs_at_station):
			psv = self.psvs_at_station[0]
			
			# drop passengers
			for passenger_seat, passenger in psv:
				if passenger.destination == self:
					psv.unload(passenger_seat)
					logger.info('Station {2}: psv {0} unloading passenger {1} station_total:{3}  psv_total{4}'.format(psv, passenger, self, len(self), len(psv)))
				
			# pick up passengers
			seats_available = psv.capacity - len(psv)
			for i in range(seats_available):
				if not len(self.waiting_area):
					break
				passenger = self.waiting_area.pop(0) 

				# add the statistic to the overall total
				# print self, passenger.spawn_time, SCL.time
				self.station_statistic_tracker.enter([SCL.time, passenger.spawn_time])

				psv.load( passenger )
				logger.info('{0} picking up {1} total_pass:{2}'.format(psv, passenger, len(psv)))

		self.time_to_next_spawn-=1

	def __len__(self):
		return len(self.waiting_area)

	def __str__(self):
		return '{1} {0} ID:{2}'.format(self.route, self.name, self._id )

class EstateMainStop(EstateBusStop):
	def __init__(self,*args, **kwargs):
		# where passengers are dropped in a subcircuit to join main circuit
		super(EstateMainStop, self).__init__(*args, **kwargs)
		# add this to the global route as well
		SCL.global_routes["MAIN"].append(self)

		# passengers are moved from circuit side waiting area to this one
		self.main_road_waiting_area = []

	def run(self):
		# drop any passengers headed for any major sinks and move them to the main_road_waiting_area
		for psv in self.psvs_at_station:
			if psv.route !="MAIN":
				for passenger_seat, passenger in psv:
					if passenger.destination in SCL.major_sinks:
						passenger = psv.unload(passenger_seat)
						self.main_road_waiting_area.append(passenger)
						logger.info('MainStation {2}: {0} unloading  {1} mainroadTotal {3} OtherWaitingArea {4}'.format(psv, passenger, self, len(self.main_road_waiting_area), len(self)))

		#move all passengers to main side before they are picked up by a matatu
		count_index = len(self.waiting_area)
		while count_index:
			count_index-=1
			passenger = self.waiting_area.pop(count_index)
			if passenger.destination in SCL.major_sinks:
				self.main_road_waiting_area.append(passenger)
				logger.info('MainStation {2}: Moving {1} mainroadTotal {3} OtherWaitingArea {4}'.format("", passenger, self, len(self.main_road_waiting_area), len(self)))
					
		# carry out usual EstateBusStop operations
		super(EstateMainStop, self).run()

		# move all passengers again to main area that have been dropped by matatu
		count_index = len(self.waiting_area)
		while count_index:
			count_index-=1
			passenger = self.waiting_area.pop(count_index)
			if passenger.destination in SCL.major_sinks:
				self.main_road_waiting_area.append(passenger)
				logger.info('MainStation {2}: Moving {1} mainroadTotal {3} OtherWaitingArea {4}'.format("", passenger, self, len(self.main_road_waiting_area), len(self)))
		
		# load passengers into mats on main road
		for psv in self.psvs_at_station:
			if psv.route == "MAIN":
				count_index = len(self.main_road_waiting_area)
				count_index -= 1
				while count_index:
					try:
						passenger = self.main_road_waiting_area.pop(count_index)
						psv.load( passenger )
						logger.info('MainStation {2}:loading {1} to {0} mainroadTotal {3} OtherWaitingArea {4}'.format(psv, passenger, self, len(self.main_road_waiting_area), len(self)))
					except IndexError, PSV.Full:
						break

	def __str__(self):
		return 'Main{1} {0} ID:{2}'.format(self.route, self.name, self._id )

class Sink(EstateBusStop):
	def __init__(self, *args, **kwargs):
		super(Sink, self).__init__( *args, **kwargs )


	def run(self):
		"""Overrides the EstateBusStop method sink it does not spawn and matatu's should not spend too much time here."""
		self.restore_passengers()

		for psv in self.psvs_at_station:
			# drop passengers
			for passenger_seat, passenger in psv:
				if passenger.destination == self:
					# todo: hold passengers, don't lose theme, until it is time to be picked up gagin
					psv.unload(passenger_seat)
					logger.info('{2}: psv {0} unloading {1} station_total:{3}  psv_total {4}'.format(psv, passenger, self, len(self), len(psv)))
				
			# pick up passengers
			seats_available = psv.capacity - len(psv)
			for i in range(seats_available):
				if not len(self.waiting_area):
					break
				passenger = self.waiting_area.pop(0) 
				psv.load( passenger )
				logger.info('{0} picking up {1} total_pass:{2}'.format(psv, passenger, len(psv)))

			#force psv leave station sink this is a sink, not a stop
			psv.at_station = False
			#remove self from station
			index= self.psvs_at_station.index(psv)
			del self.psvs_at_station[index]

	def restore_passengers(self):
		"""Puts passengers in the waiting area for pickup when it's time for 
		them to be picked up, for example, in the evening after being droppped in the morning"""
		# todo: implement
		pass

	def __len__(self):
		return len(self.waiting_area)

	def __str__(self):
		return 'Sink:{1}:{0}'.format(self.name, self._id )

class MajorSink(Sink):
	
	def __init__(self, *args, **kwargs):
		super(MajorSink, self).__init__(route="MAIN", *args, **kwargs)
		SCL.major_sinks = []
		SCL.major_sinks.append(self)
		self._id = len(SCL.major_sinks)

	def __str__(self):
		return "MajorSink:{0}".format(self.name)
		# return "MajorSink:{1}:{0}".format(self.name, self._id)

	def __repr__(self):
		return "MajorSink:{0}:{1}".format(self.__str__(), super(MajorSink, self).__repr__())
