function drawMap(){
    var gju = {};
    var plotters = [];
    gju.numberToRadius = function (number) {
        return number * Math.PI / 180;
    }

    gju.numberToDegree = function (number) {
        return number * 180 / Math.PI;
    }

    gju.pointDistance = function (pt1, pt2) {
        var lon1 = pt1[0],
            lat1 = pt1[1],
            lon2 = pt2[0],
            lat2 = pt2[1],
            dLat = gju.numberToRadius(lat2 - lat1),
            dLon = gju.numberToRadius(lon2 - lon1),
            a = Math.pow(Math.sin(dLat / 2), 2) + Math.cos(gju.numberToRadius(lat1))* Math.cos(gju.numberToRadius(lat2)) * Math.pow(Math.sin(dLon / 2), 2),
            c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return (6371 * c) * 1000; // returns meters
    }

    gju.interpolate = function (pt1, pt2, ratio) {
        var lon1 = pt1[0],
            lat1 = pt1[1],
            lon2 = pt2[0],
            lat2 = pt2[1],
            lon3 = lon1* (1-ratio) + lon2*ratio,
            lat3 = lat1* (1-ratio) + lat2*ratio;
        return [lon3, lat3];
    };

    var points = null;
    var npoints = [];
    var totalDistanceRoundCircuit = 0;
    var station_spacing = formStationSpacing; // This is given in meters

    $.getJSON("features.json", function(data) {
        // points is an array of arrays. Each of the arrays within contains longitude and latitude
        points = data["features"][0]["geometry"]["coordinates"];
        // console.log(points[0]);
        var countt = points.length;
        for (var key in points) {
            if (points.hasOwnProperty(key)) {
                key = parseInt(key);
                if(key==points.length-1) {
                    var dl = gju.pointDistance(points[key],points[0]);
                    totalDistanceRoundCircuit += dl;
                }
                else {
                    var dl = gju.pointDistance(points[key],points[key+1]);
                    totalDistanceRoundCircuit += dl;
                }
            }
        }
        var station_count = totalDistanceRoundCircuit/station_spacing;
        formNoOfStations = station_count = Math.floor(station_count);
        var actual_spacing = formStationSpacing = totalDistanceRoundCircuit/station_count;

        // Now, we loop through the existing stations, measuring out distances of actual_spacing
        npoints[0] = points[0]; // The first element remains the same
        points_count = 1;
        carry = 0;
        for (var key in points) {
            if (points.hasOwnProperty(key)) {
                key = parseInt(key);

                //last point
                if(key==points.length-1) {
                    var dl = gju.pointDistance(points[key],points[0]) + carry;
                    var gone = 0;
                    while (dl>actual_spacing) {
                        if (gone==0) {
                            var rat = (actual_spacing-carry)/(dl-carry);
                            npoints[points_count] = gju.interpolate(points[key],points[0],rat)
                        }
                        else {
                            var rat = actual_spacing/(dl);
                            npoints[points_count] = gju.interpolate(npoints[points_count-1],points[0],rat)
                        }
                        points_count += 1;
                        dl = dl - actual_spacing;
                        gone = 1;
                    }
                    carry = dl;
                }
                else {
                    // all other points except last
                    var dl = gju.pointDistance(points[key],points[key+1]) + carry;
                    var gone = 0;
                    while (dl>actual_spacing) {
                        if (gone==0) {
                            var rat = (actual_spacing-carry)/(dl-carry);
                            npoints[points_count] = gju.interpolate(points[key],points[key+1],rat)
                        }
                        else {
                            var rat = actual_spacing/(dl);
                            npoints[points_count] = gju.interpolate(npoints[points_count-1],points[key+1],rat)
                        }
                        points_count += 1;
                        dl = dl - actual_spacing;
                        gone = 1;
                    }
                    carry = dl;
                }
            }
        }

        for (var key in npoints) {
            if (npoints.hasOwnProperty(key)) {
                plotters.push(
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Point",
                            "coordinates": npoints[key]
                        },
                        "properties": {
                            "marker-symbol": "bus"
                        }
                    });
            }
        }

        L.mapbox.accessToken = 'pk.eyJ1IjoibXV1byIsImEiOiJBb3pDV0JVIn0.WQbM8nHzlNqD_40KoN-coA';

        var map = L.mapbox.map('map', 'examples.map-i86nkdio')
        .setView([-1.4484316867343656,36.9717514514923], 15)
        var featureLayer = L.mapbox.featureLayer(geojson)
        .addTo(map);
// a simple GeoJSON featureset with a single point
// with no properties
featureLayer.setGeoJSON({
    type: "FeatureCollection",
    features: [{
        type: "Feature",
        geometry: {
            type: "Point",
            coordinates: [102.0, 0.5]
        },
        properties: { }
    }]
});

        points = npoints;
    }).done(function(){
        ourSimulate(formNoOfStations, formNoOfMats, formSeed, formMatSpeed, formStationSpacing);
    }
    ); //transportSim.js
}