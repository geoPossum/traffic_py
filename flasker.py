from flask import Flask, request, redirect, render_template, url_for, jsonify
import json, os, sys, threading, traceback, urllib, ast, sets
import parse_json

import SimClass
import SimDef

app = Flask("Traffic Simulator")
# app.debug = True

parser=parse_json.Parser()
sim_results = {}

def simulator():
	SimClass.run_sim()

	SimClass.logger.info("<-----Ended at {0}--->".format( SimDef.convert_time(SimClass.time)) )
	for stop in SimClass.bus_stops:
		sim_results[stop] = []
		# print stop,"-->" , "Inner Waiting Area", len(stop.waiting_area),
		sim_results[stop].append( ["Inner Waiting Area", len(stop.waiting_area) ] )
		if isinstance(stop,SimDef.EstateMainStop):
			# print "MainRoad Waiting Area", len(stop.main_road_waiting_area)
			sim_results[stop].append( [ "MainRoad Waiting Area", len(stop.main_road_waiting_area) ] )

	# 
	import search_output_log

@app.route("/")
def index():
	try:
		SimClass.global_routes = {}
		SimClass.bus_stops = []
		SimClass.psv_s = []
		# major sinks are along the main road
		SimClass.major_sinks = []
		SimClass.sinks = []

		global parser
		#parser.get_data()
		parser.get_estate("mapbox/southbA.geojson")
		parser.get_estate("mapbox/southbB.geojson")
		parser.get_estate("mapbox/southC.geojson")	
		parser.get_trunk("mapbox/Trunk.geojson")
		
		py_vars = []
		return render_template("index.html", geofile="geofile", estates=parser.estates )

	except:
		traceback.print_exception(*sys.exc_info())
		return ('',204)

@app.route("/draw_points")
def draw_points():
	"""At this point, user has filled in form and said how many points he wants
	per estate/circuit. We do the interpolation, returning points that have coordinates, and a name and id."""
	# print estates	

	default_value = int(request.args["default_draw_value"])
	#create with default number of stops
	station_dict = {}

	try:
		# for x in parser.estates.keys():
		# 	# print x, request.args[x], request.args[x]==""
		# 	if x not in request.args:
		# 		station_dict[x] = default_value
		# 	else:
		# 		if request.args[x] == "":
		# 			station_dict[x] = default_value
		# 		else:	
		# 			station_dict[x] = int(request.args[x])

		# # print station_dict

		# parser.generate_stops(station_dict)

		array=[]
		for k,v in parser.estate_stops.iteritems():
			#v is an array of coordinates for an estate
			temp = []
			for index, coordinates in enumerate(v[0]):
				title, route, weight = "Station-{0}".format(index+1), k, 1
				if len(coordinates) == 3:
					coordinates.pop(2)
				for i in [title, route, weight]:
					coordinates.append(i)
				temp.append(coordinates)

			array.append(temp)
		return json.dumps(array)
	except:
		traceback.print_exception(*sys.exc_info())
		return ('',204)
	
@app.route("/sim_params")
def sim_params():
	global parser
	try:
		return render_template("sim_params.html", estates=parser.estates )
	except:
		traceback.print_exception(*sys.exc_info())
		return ('',204)

@app.route("/sim_config", methods=["POST"])
def sim_config():
	try:
		stops = ast.literal_eval(request.form["map_points"])
		data = {}
		for x in request.form["data"].split("&"):
			key, val = x.split("=")
			# print key, key.replace("+"," ")
			data[key.replace("+"," ")]=val
		# return None

		def myattribute(v):
			# v is [name in request, classname, parameter, default_param_value]
			request_name, module_name, param, default_val = v
			if data[request_name] == "":
				val= default_val
			else:
				val = float(data[request_name])
			setattr(sys.modules[module_name], param, val)

		# SimClass.hours_to_run = 24
		myattribute( ["hours_to_run", "SimClass", "hours_to_run", 24])

		# start at 7 am for example, by making this 7a
		# SimClass.start_time_hours_offset = 7
		myattribute( ["start_time", "SimClass", "start_time_hours_offset", 7])

		# maximum number of psvs allowed at a station at once. If this number is
		# exceeded, the psvs go past
		# SimClass.max_psvs_at_station = 4
		myattribute( ["max_psvs", "SimClass", "max_psvs_at_station", 4])

		# modify this variable in file simdef to define what
		SimDef.TimeGraph.adjusted_weight = 1
		# myattribute( ["hours_to_run", "SimClass", "hours_to_run", 1])

		# they automatically add themselves to the major_sinks list
		# todo
		SimDef.MajorSink(name="Nairobi", weight=10, index=1)
		SimDef.MajorSink(name="Mombasa Road", weight=0.05, index=3) 
		SimDef.MajorSink(name="Industrial Area", weight=1, index=2) 

		# called after creating major sinks, turns SimClass.major_sinks to a tuple that cannot be altered after
		SimClass.sink_weight_multiplier = SimDef.set_sink_multiplier() 

		routes = sets.Set()

		for stop in stops:
			# they automatically add themselves to the list
			weight, name, route, main_stop = stop

			routes.add(route)
			if main_stop:
				#where passengers shall be alighting headed to town
				SimDef.EstateMainStop(name=name, route=route,weight=weight)
			else:
				SimDef.EstateBusStop(name=name, route=route,weight=weight)
			print name,route
			

		# sort stops according to their index
		for route in SimClass.global_routes:
			SimClass.global_routes[route] = sorted(SimClass.global_routes[route],key=lambda x: x.index)
			
		# create some matatus for the sub-circuits
		# the effect of more matatus is that the earliest stop in the route has less people but since
		# mats arrive full at the next stops, those still remain high

		for route in routes:
			no_of_mats = int(data[route])
			mat_speed = 80 * 5.0/18.0 #kph to meters p sec
			if data.has_key("speed_{0}".format(route)):
				if data["speed_{0}".format(route)]!="":
					mat_speed = float(data["speed_{0}".format(route)])  * 5.0/18.0
			distance_between_stations = parser.estate_stops[route][1]
			time_between_stations = distance_between_stations/mat_speed
			for i in range(no_of_mats):
				#todo, define the time between stops here
				SimDef.Matatu(route, time_between_stations = time_between_stations)

		SimDef.assign_psvs()

		return ('',204)

	except:
		traceback.print_exception(*sys.exc_info())
		return ('',204)

@app.route("/run_sim")
def run_simulation():
	try:
		threading.Thread(target=simulator).start()
		return ('',204)
		# return jsonify(time=SimClass.time/float(SimClass.end_time))
	except:
		traceback.print_exception(*sys.exc_info())
		return ('',204)

@app.route("/query_sim")
def query_progress():
	try:
		return jsonify(time=float("{0:.2f}".format(100*(SimClass.time/float(SimClass.end_time)))))
	except:
		traceback.print_exception(*sys.exc_info())
		return ('',204)

@app.route("/get_results")
def get_results():
	sim_results={}
	for stop in SimClass.bus_stops:
		print stop
		sim_results[stop] = []
		# print stop,"-->" , "Inner Waiting Area", len(stop.waiting_area),
		sim_results[stop].append( ["Waiting at Bus Stop", len(stop.waiting_area) ] )
		if isinstance(stop,SimDef.EstateMainStop):
			# print "MainRoad Waiting Area", len(stop.main_road_waiting_area)
			sim_results[stop].append( [ "Waiting on Main Cct", len(stop.main_road_waiting_area) ] )
	print "SIM Results:", sim_results

	return render_template("results.html",results=sim_results, col1_title="Station ID", col2_title="Side(Main Route or Estate)", col3_title="Passengers On Platform at End of Sim")


@app.route("/reset_sim")
def reset_sim():
	try:
		SimClass.global_routes = {}
		SimClass.bus_stops = []
		SimClass.psv_s = []
		# major sinks are along the main road
		SimClass.major_sinks = []
		SimClass.sinks = []
		return redirect(url_for('.index'))
	except:
		traceback.print_exception(*sys.exc_info())
		return ('',204)

if __name__=="__main__":
    port = int(os.environ.get("PORT", 5000))
    app.run(host='0.0.0.0', port=port)
