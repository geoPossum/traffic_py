import json
from math import radians, cos, sin, asin, sqrt, atan2, degrees, pi

class Parser(object):
	@staticmethod
	def get_distance(coord1, coord2):
	    """
	    Calculate the great circle distance between two points 
	    on the earth (specified in decimal degrees)"""
	    
	    # convert decimal degrees to radians 
	    lon1, lat1 = coord1[:2]
	    lon2, lat2 = coord2[:2]
	    
	    # haversine formula 
	    dlon = lon2 - lon1 
	    dlat = lat2 - lat1 
	    lat1, lat2, dlon, dlat = map(radians, [float(lat1), float(lat2), float(dlon), float(dlat) ])

	    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
	    c = 2 * asin(sqrt(a)) 
	    r = 6371 # Radius of earth in kilometers. Use 3956 for miles

	    #get bearing
	    y = sin(dlon) * cos(lat2)
	    x = cos(lat1)*sin(lat2) - sin(lat1)*cos(lat2)*cos(dlon)
	    z = atan2(y, x)
	    return c * r * 1000

	@staticmethod
	def interpolate(pt1, pt2, ratio):
		lon1 = pt1[0]
		lat1 = pt1[1]
		lon2 = pt2[0]
		lat2 = pt2[1]
		lon3 = lon1* (1-ratio) + lon2*ratio
		lat3 = lat1* (1-ratio) + lat2*ratio
		# print "X",[lon3, lat3]
		# print pt1,"*",pt2,"*",ratio,"*", (1-ratio),"*",lon1* (1-ratio),"*",lon2*ratio, "*",lat1* (1-ratio),"*",lat2*ratio
		return [lon3, lat3]


	def __init__(self):

		self.estates = {}
		self.estate_stops = {}
		self.sinks = {}
		self.main_route = []

	def get_data(self, geofile=None):
		"""Extract data from the json file"""
		if not geofile:
			geofile = "mapbox/features.json"
		with open(geofile) as w:
			json_contents = json.loads(w.read())

			features = json_contents["features"]

			for i in features:
				if i.has_key("geometry"):
					geometry = i["geometry"]
					#print geometry
					properties = i["properties"]
					coordinates = geometry["coordinates"]
					description = properties["description"]
					title = properties["title"]

				else:
					print i
					continue

				if len(description)!=0:
					if description.lower() == "estate":
						self.estates[title] = coordinates
					elif description.lower() == "sink":
						self.sinks[title] = coordinates
					elif description.lower() == "mainroute":
						self.mainroute = coordinates

				# print title
				# print type_,  description,"-", title
			return self.estates, self.sinks
	
	def get_estate(self, geofile):	
		with open(geofile) as w:
			json_contents = json.loads(w.read())

			features = json_contents["features"]
			stations = []
			co_no = total_distance = 0
			for i in features:
				if i.has_key("geometry"): 
					geometry = i["geometry"]
					#print geometry	
					properties = i["properties"]
					coordinates = geometry["coordinates"]
					title = properties["Name"]
					if geometry["type"] == "Polygon":	
						# We append the new route to the list of estates
						for index in range(len(coordinates[0]) - 1):	
							distance = Parser.get_distance( coordinates[0][index], coordinates[0][index + 1] )
							total_distance += distance	
						co_no = len(coordinates[0])
						self.estates[title] = coordinates
						estate = title
					elif geometry["type"] == "Point":
						# We add a new stop to the route here... We'll ignore naming for now... add it a little later.
						stations += [coordinates]	
				else:
					print i, " has no geometry."
					continue
			actual_spacing = total_distance / float(co_no)
			# Next, we add the stops to the blah
			self.estate_stops[estate] = [stations, actual_spacing]	
			print "Estate %s added with %d stations." % (estate, len(stations))
			return self.estates
	
	def get_trunk(self, geofile):	
		with open(geofile) as w:
			json_contents = json.loads(w.read())

			features = json_contents["features"]
			station_count = 0
			for i in features:
				if i.has_key("geometry"): 
					geometry = i["geometry"]
					#print geometry	
					properties = i["properties"]
					coordinates = geometry["coordinates"]
					title = properties["Name"]
					if geometry["type"] == "LineString":	
						# We append the new route to the list of estates	
						self.mainroute = coordinates
						estate = title
					elif geometry["type"] == "Point":
						# We add a new stop to the route here... We'll ignore naming for now... add it a little later.
						self.sinks[title] = coordinates
						station_count += 1
				else:
					print i, " has no geometry."
					continue
			print "Main Trunk added with %d sinks." % (station_count,)
			return self.estates

	def generate_stops(self, station_no):
		assert type(station_no) == dict
		for station in station_no.keys():
			assert station in self.estates.keys()

		self.estate_stops = {}

		for station, no in station_no.iteritems():
			coordinates = self.estates[station][0]
			total_distance = 0
			for index in range(len(coordinates)-1):
				distance = Parser.get_distance( coordinates[index], coordinates[index+1] )
				total_distance += distance


			# no-=1
			# print total_distance
			actual_spacing = total_distance / no
			spacing_for_point = actual_spacing

			# switch latitude for longitude
			npoints = []
			npoints.append(coordinates[0]) #the first stop is always included

			points_count = 1
			carry = 0

			for key in range(len(coordinates)):
				current_index = key
				next_index = (key+1)%len(coordinates)
				# print current_index, next_index, "--->", coordinates[current_index], "--->", coordinates[next_index]
	    
				dl = self.get_distance(coordinates[current_index], coordinates[next_index]) + carry
				gone = 0
				while dl > actual_spacing:

					if not gone:
						rat = float(actual_spacing-carry)/float(dl-carry)
						new_point = self.interpolate(coordinates[current_index], coordinates[next_index], rat)
						# print "-->",dl," * ",carry," * ",rat," * ",new_point
					else:
						rat = actual_spacing/float(dl)
						new_point = self.interpolate(npoints[points_count-1], coordinates[next_index], rat)
						# print "-->",dl," * ",carry," * ",rat," * ",new_point
						
					npoints.append( new_point )
					points_count+=1
					dl = dl - actual_spacing
					gone = 1
				carry = dl

			self.estate_stops[station] = [npoints, actual_spacing]
			
			if len(npoints)>no and npoints[-1] == npoints[0]:
				del npoints[-1]
				# print "deleted"
			# print station, len(npoints)
			# return

if __name__=="__main__":
	parser = Parser()
	# estates, _ = parser.get_data()
	# del estates["South B"]
	# x=estates["South C"]
	# print x[0]
	
	# station_no = { x: 10 for x in estates.keys() }
	# print station_no
	# parser.generate_stops(station_no)
	
	# # I'm lazy... so we'll just test everything from here
	
	import SimClass
	import SimDef
	
	SimClass.global_routes = {}
	SimClass.bus_stops = []
	SimClass.psv_s = []
	# major sinks are along the main road
	SimClass.major_sinks = []
	SimClass.sinks = []
	
	parser.get_estate("mapbox/southbA.geojson")
	parser.get_estate("mapbox/southbB.geojson")
	parser.get_estate("mapbox/southC.geojson")	
	parser.get_trunk("mapbox/Trunk.geojson")
