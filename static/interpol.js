function drawMap(map, points){
    var gju = {};
    var plotters = [];
    gju.numberToRadius = function (number) {
        return number * Math.PI / 180;
    }

    gju.numberToDegree = function (number) {
        return number * 180 / Math.PI;
    }

    gju.pointDistance = function (pt1, pt2) {
        var lon1 = pt1[0],
            lat1 = pt1[1],
            lon2 = pt2[0],
            lat2 = pt2[1],
            dLat = gju.numberToRadius(lat2 - lat1),
            dLon = gju.numberToRadius(lon2 - lon1),
            a = Math.pow(Math.sin(dLat / 2), 2) + Math.cos(gju.numberToRadius(lat1))* Math.cos(gju.numberToRadius(lat2)) * Math.pow(Math.sin(dLon / 2), 2),
            c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return (6371 * c) * 1000; // returns meters
    }

    gju.interpolate = function (pt1, pt2, ratio) {
        var lon1 = pt1[0],
            lat1 = pt1[1],
            lon2 = pt2[0],
            lat2 = pt2[1],
            lon3 = lon1* (1-ratio) + lon2*ratio,
            lat3 = lat1* (1-ratio) + lat2*ratio;
        return [lon3, lat3];
    };

    var npoints = [];
    var totalDistanceRoundCircuit = 0;
    var station_spacing = 10; // This is given in meters

    // console.log(points[0]);
    var countt = points.length;
    for (var key in points) {
        if (points.hasOwnProperty(key)) {
            key = parseInt(key);
            if(key==points.length-1) {
                var dl = gju.pointDistance(points[key],points[0]);
                totalDistanceRoundCircuit += dl;
            }
            else {
                var dl = gju.pointDistance(points[key],points[key+1]);
                totalDistanceRoundCircuit += dl;
            }
        }
    }
    var station_count = totalDistanceRoundCircuit/station_spacing;
    formNoOfStations = station_count = Math.floor(station_count);
    var actual_spacing = formStationSpacing = totalDistanceRoundCircuit/station_count;

    // Now, we loop through the existing stations, measuring out distances of actual_spacing
    npoints[0] = points[0]; // The first element remains the same
    points_count = 1;
    carry = 0;
    for (var key in points) {
        if (points.hasOwnProperty(key)) {
            key = parseInt(key);

            //last point
            if(key==points.length-1) {
                var dl = gju.pointDistance(points[key],points[0]) + carry;
                var gone = 0;
                while (dl>actual_spacing) {
                    if (gone==0) {
                        var rat = (actual_spacing-carry)/(dl-carry);
                        npoints[points_count] = gju.interpolate(points[key],points[0],rat)
                    }
                    else {
                        var rat = actual_spacing/(dl);
                        npoints[points_count] = gju.interpolate(npoints[points_count-1],points[0],rat)
                    }
                    points_count += 1;
                    dl = dl - actual_spacing;
                    gone = 1;
                }
                carry = dl;
            }
            else {
                // all other points except last
                var dl = gju.pointDistance(points[key],points[key+1]) + carry;
                var gone = 0;
                while (dl>actual_spacing) {
                    if (gone==0) {
                        var rat = (actual_spacing-carry)/(dl-carry);
                        npoints[points_count] = gju.interpolate(points[key],points[key+1],rat)
                    }
                    else {
                        var rat = actual_spacing/(dl);
                        npoints[points_count] = gju.interpolate(npoints[points_count-1],points[key+1],rat)
                    }
                    points_count += 1;
                    dl = dl - actual_spacing;
                    gone = 1;
                }
                carry = dl;
            }
        }
    }

    for (var key in npoints) {
        if (npoints.hasOwnProperty(key)) {
            plotters.push(
                {
                    "type": "Feature",
                    "geometry": {
                        "type": "Point",
                        "coordinates": npoints[key]
                    },
                    "properties": {
                        "marker-symbol": "bus"
                    }
                });
        }
    }

    L.mapbox.accessToken = 'pk.eyJ1IjoiZ2Vvbnlvcm8iLCJhIjoiaF9wT01LWSJ9.EVhsqQht3EA-zLW0UQBvng';

    var map = L.mapbox.map('map', 'geonyoro.loj2ddop')
    .setView([-1.315,36.829], 15)
    // var featureLayer = L.mapbox.featureLayer(geojson)
    // .addTo(map);
    for (var key in npoints) {
        if (npoints.hasOwnProperty(key)) {
        }
    }

    points = npoints;
}