import SimClass
import SimStats
import SimDef
import os,sys


SimClass.hours_to_run = 3
# start at 7 am for example, by making this 7a
SimClass.start_time_hours_offset = 7

# maximum number of psvs allowed at a station at once. If this number is
# exceeded, the psvs go past
SimClass.max_psvs_at_station = 4

# modify this variable in file simdef to define what
SimDef.TimeGraph.adjusted_weight = 1

# they automatically add themselves to the major_sinks list
SimDef.MajorSink(name="Nairobi", weight=10, index=1)
SimDef.MajorSink(name="Mombasa Road", weight=0.05, index=3) 
SimDef.MajorSink(name="Industrial Area", weight=1, index=2) 

# called after creating major sinks, turns SimClass.major_sinks to a tuple that cannot be altered after
SimClass.sink_weight_multiplier = SimDef.set_sink_multiplier() 

#creation of some weights in the sim
l = [0.2,1,0.5,0.1]

# create 4 stops
for i in range(4):
	# they automatically add themselves to the list
	SimDef.EstateBusStop(name='South-C', route='South-C',weight=l[i])
	SimDef.EstateBusStop(name='South-B', route='South-B',weight=l[i])

#where passengers shall be alighting
SimDef.EstateMainStop(name="MainStop",route='South-C',weight=0.04)
SimDef.EstateMainStop(name="MainStop",route='South-B',weight=0.04)


# sort stops according to their index
for route in SimClass.global_routes:
	SimClass.global_routes[route] = sorted(SimClass.global_routes[route],key=lambda x: x.index)
	
# create some matatus for the sub-circuits
# the effect of more matatus is that the earliest stop in the route has less people but since
# mats arrive full at the next stops, those still remain high
for i in range(20):
	SimDef.Matatu('South-C', 240)

# create 2 matatus for Mombasa Road
for i in range(5):
	SimDef.Matatu('MAIN', 500)

SimDef.assign_psvs()

SimClass.run_sim()

SimClass.logger.info("<-----Ended at {0}--->".format( SimClass.convert_time(SimClass.time)) )
for stop in SimClass.bus_stops:
	print stop,"-->" , "Inner Waiting Area", len(stop.waiting_area),
	if isinstance(stop,SimDef.EstateMainStop):
		print "MainRoad Waiting Area", len(stop.main_road_waiting_area)
	else:
		print


for i in SimDef.global_station_statistics:
	print i.summarize()
